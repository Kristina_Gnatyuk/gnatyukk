package Lab5;

import java.io.PrintStream;

/**
 * Created by gnatyuk on 25-Apr-16.
 */
public class MyException2 extends RuntimeException {
    /**
     * Prints this throwable and its backtrace to the specified print stream.
     *
     * @param s {@code PrintStream} to use for output
     */
    @Override
    public void printStackTrace(PrintStream s) {
        System.out.println("Выходной день");
    }
}
