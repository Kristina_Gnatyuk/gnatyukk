package Lab5;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

/**
 * Created by gnatyuk on 25-Apr-16.
 */
public class Main {
    public static void main(String[] args) {

        MyList list = new MyList();

        list.add("2016-03-20");
        list.add("2016-03-21");
        list.add("2016-03-22");
        list.add("2016-03-23");
        list.add("2016-03-24");
        list.add("2016-03-25");
        list.add("2016-03-26");
        list.add("2016-03-27");
        list.add("2016-03-28");
        list.add("2016-03-29");
        list.add("2016-03-30");
        list.add("2016-03-31");
        list.add("2016-04-01");
        list.add("2016-04-02");
        list.add("2016-04-03");
        list.add("2016-04-04");
        list.add("2016-04-05");
        list.add("2016-04-06");
        list.add("2016-04-07");
        list.add("2016-04-08");
        list.add("2016-04-09");
        list.add("2016-04-10");
        list.add("2016-04-11");
        list.add("2016-04-12");
        list.add("2016-04-13");
        list.add("2016-04-14");
        list.add("2016-04-15");
        list.add("2016-04-16");
        list.add("2016-04-17");
        list.add("2016-04-18");


        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i) + " - ");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd"); // формат передаваемой даты
            Date date = null;
            try {
                date = format.parse(String.valueOf(list.get(i)));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date); // настраиваем календарь передав дату


            if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
                try {
                    throw new MyException2();
                } catch (MyException2 e) {
                    e.printStackTrace();
                }

            } else {
                try {
                    throw new MyException();
                } catch (MyException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}





