package Lab2;

/**
 * Created by gnatyuk on 06-Apr-16.
 */
public class Task2 {
    public static void main(String[] args)
    {
        short sh1 = 5;
        String r1 = Short.toString(sh1);
        System.out.println(r1);

        String r2 = "6";
        short sh2 = Short.parseShort(r2);
        System.out.println(sh2);


        byte b1 = 20;
        String a1 = Byte.toString(b1);
        System.out.println(a1);

        String a2 = "100";
        byte b = Byte.parseByte(a2);
        System.out.println(b);


        int i = 555;
        String str = Integer.toString(i);
        System.out.println(str);

        String str2 = "1000";
        int i2 = Integer.parseInt(str2);
        System.out.println(i2);


        double  x = 11.11;
        String s = Double.toString(x);
        System.out.println(s);

        String s2 = "0.15";
        double x2 = Double.parseDouble(s2);
        System.out.println(x2);


        long  l1 = 567345464;
        String z = Long.toString(l1);
        System.out.println(z);

        String z2 = "333333333";
        long l2 = Long.parseLong(z2);
        System.out.println(l2);


        float  f1 = 3.46f;
        String y1 = Float.toString(f1);
        System.out.println(y1);

        String y2 = "36.6";
        float f2 = Float.parseFloat(y2);
        System.out.println(f2);


        char ch = 'S';
        String m = Character.toString(ch);
        System.out.println(m);

        String m2 = "hello";
        char ch2[] = m2.toCharArray();
        System.out.println(ch2);


        boolean bool = true;
        String string = Boolean.toString(bool);
        System.out.println(string);

        String s11 = "True";
        String s12 = "yes";
        boolean bool1;
        boolean bool2;

        bool1 = Boolean.parseBoolean(s11);
        bool2 = Boolean.parseBoolean(s12);
        System.out.println(bool1);
        System.out.println(bool2);


    }
}
