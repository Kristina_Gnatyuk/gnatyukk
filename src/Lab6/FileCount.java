package Lab6;

import java.io.File;

public class FileCount {

    public int fileCount(String path)
    {
        File file;
        File[] s;
        int c = 0;

        file = new File(path);
        s = file.listFiles();
        for(int j=0;j<s.length;j++)
        {
            c++;
            if(s[j].isDirectory())
                fileCount(s[j].getPath());
        }
        return c;
    }
}
