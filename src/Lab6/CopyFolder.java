package Lab6;

import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CopyFolder implements Runnable {
    static int n = 0;

    File srcFolder = new File("F:\\Gnatyuk\\NewProject\\gnatyukk\\gnatyukk\\Folder");
    File destFolder = new File("F:\\Gnatyuk\\NewProject\\gnatyukk\\gnatyukk\\Folder" + n);
    File src = new File(srcFolder, "txt");
    File dest = new File(destFolder, "txt");


    public static List<String> getFileNames(File directory, String extension) {

        List<String> list = new ArrayList<String>();
        File[] total = directory.listFiles();
        for (File file : total) {

            if (file.getName().endsWith(extension)) {
                list.add(file.getName());

           }
           if (file.isDirectory()) {
                List<String> tempList = getFileNames(file, extension);
                list.addAll(tempList);
           }
        }
        return list;
    }




    public static void main(String[] args) {

        FileCount file = new FileCount();
        int i = file.fileCount("F:\\Gnatyuk\\NewProject\\gnatyukk\\gnatyukk\\Folder");

        System.out.println("Введите имя файла");

        for(n = 1; n <= i; n++) {

            Runnable copFold = (Runnable) new CopyFolder();
            Thread cop = new Thread(copFold);
            cop.start();
        }
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        copy(src, dest);

    }

    public void copy(File source, File destination) {
        Scanner scanner = new Scanner(System.in);
        String userText = scanner.nextLine();


        List<String> files = getFileNames(new File("F:\\Gnatyuk\\NewProject\\gnatyukk\\gnatyukk\\Folder"), "txt");

        for (String string : files) {

            if (string.indexOf(userText) >= 0) {

                File src = new File("F:\\Gnatyuk\\NewProject\\gnatyukk\\gnatyukk\\Folder", string);
                File dest = new File(destFolder, string);

                try {
                    destFolder.mkdirs();
                    Files.copy(src.toPath(), dest.toPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
//            else {
//                System.out.println("Введенный текст не соответствует названию файла");
//            }
        }
    }
}
