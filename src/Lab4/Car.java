package Lab4;


public abstract class Car {
    double m; // маневренность
    double a; // ускорение, м/с
    double Vmax; // максимальная скорость, м/с
    double V0; // начальная скорость
    double S = 2000;

    public int totalTime;

    Car() {
        this.V0 = 0;
    }

    public abstract void turn();


    void drive() {

        double tStart;
        double tEnd;
        double t;
        double S1; // отрезок до набора максимальной скорости
        double t1; // время до набора максимальной скорости
        double t2; // время движения на максимальной скорости

        int i = 0;
        while (i < 20) {
            double Vturn = 0.0;
            S1 = (((Math.pow(Vmax, 2)) - (Math.pow(V0, 2))) / (2 * a));
            if (S1 > 2000) {
                Vturn = Math.sqrt((Math.pow(V0, 2)) + (2 * a * 2000));
                t1 = (Vturn - V0) / a;
                t2 = 0;
            } else {
                Vturn = Vmax;
                t1 = (Vturn - V0) / a;
                t2 = (2000 - S1) / Vmax;
            }

            totalTime += (int)t1 + (int)t2;

            turn();

            i++;

        }
    }
}






