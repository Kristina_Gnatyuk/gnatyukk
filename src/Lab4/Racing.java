package Lab4;
import java.util.*;


public class Racing {
    public static void main(String[] args) {



        Car car1 = new Car1();
        car1.drive();

        Car car2 = new Car2();
        car2.drive();

        Car car3 = new Car3();
        car3.drive();


        Car [] cars = {car1, car2, car3};

        Car[] sorted = bubbleSort(cars);
        for(Car car : sorted)
        {
            int minut = car.totalTime / 60;
            int second = car.totalTime % 60;
            System.out.println("Автомобиль " + (Arrays.asList(cars).indexOf(car) + 1) + " проехал за " + minut + " минут " + second + " секунд");

        }

    }


    public static Car[] bubbleSort(Car[] arr) {
        Car[] newar = new Car[arr.length];
        for (int i=0; i<arr.length; i++)
        {
            newar[i] = arr[i];
        }

        for (int i = 0; i < newar.length; i++) {
            for (int j = 0; j < i; j++) {
                if (newar[j].totalTime > newar[j + 1].totalTime) {
                    Car tmp = newar[j];
                    newar[j] = newar[j + 1];
                    newar[j + 1] = tmp;
                }
            }
        }

        return newar;
    }
}

