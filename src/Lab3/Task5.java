package Homework.Lab3;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by gnatyuk on 08-Apr-16.
 */
public class Task5 {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int i = 0;
        String sN = "s";

        while (!sN.equals("Sum")){
            System.out.println("Enter number or word Sum");
            sN = reader.readLine();
            if (!sN.equals("Sum")){
                int iN = Integer.parseInt(sN);
                i=i+iN;
            }else {
                System.out.println("Sum is " + i);
            }
        }

    }

}