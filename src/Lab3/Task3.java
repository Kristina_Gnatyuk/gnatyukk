package Homework.Lab3;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by gnatyuk on 08-Apr-16.
 */
public class Task3 {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter number more than 1, please");
        String sN = reader.readLine();
        int nN = Integer.parseInt(sN);

        if (nN < 2) {
            System.out.println("There is no prime numbers in given interval. Please try again");

        }

        for (int a = 2; a <= nN; a++) {
            int i = 0;
            for (int b = 1; b <= nN; b++) {
                if (a % b == 0) {
                    i++;
                }
            }
            if (i == 2) {
                System.out.println(a);
            }
        }
    }
}